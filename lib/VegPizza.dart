
import 'dart:core';
import 'package:flutter/material.dart';



class VegPizza extends StatefulWidget {
  const VegPizza({Key? key}) : super(key: key);

  @override
  _VegPizzaState createState() => _VegPizzaState();
}

class _VegPizzaState extends State<VegPizza> {


  String   str1="Veggie_Paradise",s2="peppy_paneer",s3="Fresh_Veggie",s4="Cheese_Corn",s5="Deluxe_veggie",s6="Green_Wave",s7="Margherita",s8="Cheese_N_Corn",s9="Paneer_Makhani",s10="Indi_Tandoori_paneer";
  var p1,s1,c,v1,tot1=0,tot2=0,tot3=0,tot4=0,tot5=0,tot6=0,tot7=0,tot8=0,tot9=0,tot10=0,sum=0;
  String pro1="",pro2="",pro3="",pro4="",pro5="",pro6="",pro7="",pro8="",pro9="",pro10="";

  TextEditingController inp1=new TextEditingController();
  TextEditingController inp2=new TextEditingController();
  TextEditingController inp3=new TextEditingController();
  TextEditingController inp4=new TextEditingController();
  TextEditingController inp5=new TextEditingController();
  TextEditingController inp6=new TextEditingController();
  TextEditingController inp7=new TextEditingController();
  TextEditingController inp8=new TextEditingController();
  TextEditingController inp9=new TextEditingController();
  TextEditingController inp10=new TextEditingController();


  int change(int n1,p1)
  {
    setState(() {
      s1=n1*p1;

    });
    return s1;

  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
      home: Scaffold(

        body: Center(
          child: ListView(
            children: <Widget>[
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$str1'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:100rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp1,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot1 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp1.text);
                                  tot1=change(v1,100);
                                  pro1=str1;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s2'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:145 rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp2,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot2 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp2.text);
                                  tot2=change(v1,145);
                                  pro2=s2;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),//end
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s3'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:150 rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp3,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot3 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp3.text);
                                  tot3=change(v1,150);
                                  pro3=s3;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s4'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:140 rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp4,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot4 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp4.text);
                                  tot4=change(v1,140);
                                  pro4=s4;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),//end
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s5'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:150 rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp5,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot5 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp5.text);
                                  tot5=change(v1,150);
                                  pro5=s5;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),//end
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s6'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:160 rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp6,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot6 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp6.text);
                                  tot6=change(v1,160);
                                  pro6=s6;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),//end
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s7'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:170rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp7,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot7 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp7.text);
                                  tot7=change(v1,170);
                                  pro7=s7;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),

              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s7'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:190rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp7,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot8 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp8.text);
                                  tot8=change(v1,190);
                                  pro8=s8;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s9'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:250rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp9,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot9 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp9.text);
                                  tot9=change(v1,250);
                                  pro9=s9;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: 400,
                height: 250,
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                  child: Column(
                    children: <Widget>[
                      Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                          child: Image.asset('assets/images/pizza1.jpeg',height: 120,width: 300,)),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          child: Text('$s10'),
                        ),

                      ),
                      Container(
                        width: 350,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:<Widget> [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Text('Price:300rs '),
                              ),

                            ),
                            Container(

                              width: 50,
                              height: 20,
                              child: TextField(
                                controller: inp10,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(

                                  ),
                                ) ,
                              ),
                            ),
                            Container(
                              child: Text('Amt: $tot10 rs'),
                            ),
                            FloatingActionButton(
                              child: Text('Add'),
                              splashColor: Colors.red,
                              onPressed: (){
                                setState(() {

                                  v1=int.parse(inp10.text);
                                  tot10=change(v1,300);
                                  pro10=s10;

                                });},

                            ),
                          ],

                        ),

                      )
                    ],
                  ),
                ),
              ),




              Container(
                child: Center(
                  child: FloatingActionButton(
                    child: Text('pay'),
                    onPressed: (){
                      setState(() {
                        sum =tot1+tot2+tot3+tot4+tot5+tot6+tot7+tot8+tot9+tot10;
                      });

                      Navigator.push(context, MaterialPageRoute(builder: (context)=>Display(tot:sum,pr1:pro1,pr2:pro2,pr3:pro3,pr4:pro4,pr5:pro5,pr6:pro6,pr7:pro7,pr8:pro8,pr9:pro9,pr10: pro10)),);

                    },
                  ),
                ),
              )
            ],

          ),
        ),
      ),
    );

  }
}
class Display extends StatelessWidget {
  final tot;
  final String p=" Your products here";
  final pr1,pr2,pr3,pr4,pr5,pr6,pr7,pr8,pr9,pr10;


  Display(
      {@required this.tot ,this.pr1,this.pr2,this.pr3,this.pr4,this.pr5,this.pr6,this.pr7,this.pr8,this.pr9,this.pr10});




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 400,
          height: 500,
          child: Card(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

            child: Column(
              children: <Widget>[
                Card(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),

                    child: Image.asset("assets/images/pizza1.jpeg",height: 120,width: 300,)),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Center(
                    child: Expanded(
                      child: Container(


                        child: Text('  $p   \n \n   $pr1  \n  $pr2  \n  $pr3  \n  $pr4  \n  $pr5  \n  $pr6  \n  $pr7  \n $pr8  \n $pr9  \n $pr10  \nTotal_Amount: $tot Rs \n ' ),

                      ),
                    ),
                  ),

                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
