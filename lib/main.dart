import 'package:flutter/material.dart';
import 'package:pizzacap/BurgerType.dart';
import 'package:pizzacap/Contact.dart';
import 'package:pizzacap/VegPizza.dart';
import 'package:pizzacap/NonVeg_Pizzas.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String _title = '';
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MyStatelessWidget(),
    );
  }
}

class MyStatelessWidget extends StatelessWidget {

  const MyStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 3,
      child: Scaffold(
        drawer: Drawer(
          child: ListView(

            padding: EdgeInsets.all(10.0),
            children: <Widget>[
              UserAccountsDrawerHeader(accountName: Text('Pizza_Corner'), accountEmail: Text('PizzaCorner@gmail.com'),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.black,
                  child: Text('Pizza'),
                ),

              ),

              ListTile(

                leading: Icon(Icons.location_on_sharp),
                title: Text('Location'),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Contact()),);
                },
              ),
              ListTile(

                leading: Icon(Icons.home),
                title: Text('Home'),
                onTap: (){
                  Navigator.pop(context);

                },
              ),
              ListTile(
                leading: Icon(Icons.search),
                title: Text('search'),
                onTap: (){


                },

              ),
              ListTile(

                leading: Icon(Icons.broken_image_rounded),
                title: Text('Contact us'),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>VegPizza()),);
                },
              ),
              ListTile(

                leading: Icon(Icons.logout),
                title: Text('Logout'),
                onTap: (){

                },
              )
            ],

          ),


        ),
        appBar: AppBar(
          title: const Text('Pizza  Corner',style: TextStyle(fontSize: 30,color: Colors.white),),
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.local_pizza_outlined),
                child: Text('Veg_pizzas'),
              ),
              Tab(
                icon: Icon(Icons.local_pizza_rounded),
                child: Text('Non_Veg_pizzas'),
              ),
              Tab(
                icon: Icon(Icons.local_pizza_sharp),
                child: Text('Burger_Pizzas'),
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: <Widget>[
            Center(
              child: VegPizza(),

            ),
            Center(
              child: NonVeg_Pizza(),
            ),
            Center(
              child: BugerType(),
            ),
          ],
        ),
      ),
    );

  }
}
